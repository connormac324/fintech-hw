# Fintech HW

--
# PayPal

## Company Profile

* PayPal one of the forefathers of the fintech industry
Now a conglomerate of the financial services industry with 21,800 employees and a revenue of $17.772 Billion in 2019.

* Dan Schulman- current President & CEO

* Founded 1998 with an IPO in 2002

* Founded by forward thinkers including Elon Musk and Peter Theil

* A need was rising in the late 90's with the 'dot com' boom and rise of E-commerce to have a way to make secure payments over the internet. PayPal answered this need by providing a software that enables purchases without compromising personal data.

* The comapny struggled initially until focusing on the money transfer portion of the business. At which point it had an IPO at $13 per share raising $61 million.


## Business Activities:

* The company is primarily focused on providing a financial service to a growing need for a secure way to transfer funds in cyberspace.

* The company's intended customer is to provide seller to buyer services with a secure means to conduct financial transactions without exposing a parties information. For example, on Ebay when a trasaction is made PayPal is available to use as a safe and secure means to ensure the transaction takes place providing ease of mind to both buyer and seller. Also, as individuals launch their own service websites through Shopify or fiverr they provide a means for smaller businesses to conduct business. However banks and credit card companys have seen the market and are now providing services to compete.

PayPal acts as a wallet. Containning credit card, debit card and banking information. Through PayPal that information can be safely relayed for a transaction without exposing a parties confidential information such as the bank account number, experation dates and security codes. For sellers it offers a reliable way to collect funs from transactions into a single PayPal account through their multiple services provided to sellers of all sizes.

* Paypal uses software to enable Automated Clearing House transfers between buyer and sellers as a service instead of parties having to take risks with personal information in the private transaction area. 


## Landscape:

* This company has brought forward the industry of virtual transactions. Defined the landscape and brought a service to the need of virtual transactions. Currently more and more transactions become virtual and occour in the cyberspace.

* The company answered the need for online payments. The trend has been to bring ease of payment for day to day transactions. For interpersonnel payments which the company is answering through its subsidiary Venmo (which is not yet profitable).

* Companies such as ChasApp and now Apple with ApplePay and credit card companies as well as banks have seen not only the oppourtunity but the need to compete and have services in this space. One of PayPal's greatest threats would be individual websites providing their own secure payment method to challenge PayPal. However this would provide minimal benefit to the company as the software exsist at low or no cost to the company hosting the transaction currently. The cost of programming and establishing the secure payment method would not justify the reward.


## Results

* This company has revolutionalized how transactions are made online by making a secure method for the transaction occour and in person through their subsidiaries. They have been able to introduce and largely control a multi Billion dollar transaction market.

* There are several core metrics used to measure a companies success in this niche of fintech. To include number of users, number of transactions, dollar figure for amount of funds transffered. In all of these areas PayPal and its subsidiaries have a strong hold on the market. However they face rising competition from companies such as CashApp,

* The company is doing well two decades after their inception. Aquiring numerous other companies and inreasing their revenue by over $17.7 Billion dollars. The growing E-commerce industry strongly supports the need for the services they provide. Ultimately this company has a strong outlook with many growing business oppourtunities going forward.


## Recommendations

* I would advise them to get an app to compete with Apple's wallet and their auto pay using their phone.

A rise in concern of public health related to the COVID pandemic and exisiting trends in the secotr to virtualize and automate payments make this a prime area for PayPal to seek an inroad in with their exsistung infastructure. Auomtaing payments and enabling the convience of paying from a smartphone would eliminate the need to carry a wallet and ultimately provide the peace of mind to have secure payment methods quickly anywhere.

This would help the company break into more sectors of transactions. They have aquired Venmo to compete in the personal transaction space which heps them greatly expand their territory in the financial transaction area.

To implement this technology would require the use of a program run by businesses and downloadable on smartphones to link and extract information from the right consumer to the right vendor for the correct amount.



Works Cited:

Trautman, Lawrence J. "E-Commerce, cyber, and electronic payment system risks: lessons from PayPal." UC Davis Bus. LJ 16 (2015): 261

Williams, Damon. "Introduction to PayPal." Pro PayPal E-Commerce (2007): 1-12.

Preibusch, Sören, et al. "Shopping for privacy: Purchase details leaked to PayPal." Electronic Commerce Research and Applications 15 (2016): 52-64.

Levchin, Max, et al. "System and method for electronically exchanging value among distributed users." U.S. Patent No. 7,089,208. 8 Aug. 2006.

Eisenmann, Thomas R., and Lauren Barley. "PayPal merchant services." (2006).

https://www.paypal.com/us/home


